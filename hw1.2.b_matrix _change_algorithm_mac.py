import numpy as np
import time

'''
Platform One

Hardware & Software environment: 
CPU: Intel i9-9880H     8 cores @ 2.3Ghz Turbo Boost 4.8Ghz 
Memory: 16GB @ 2666 Mhz 
OS: MacOs
IDE: PyCharm 2020.2 Professional
Interpreter: Python 3.7

'''


# generates 2 integer matrices and multiply several times, record execution time
# Idea: use for loop instead of numpy method

# Integer matrix

def matrixMultiply(A, B):
    result = [[0] * len(B[0]) for i in range(len(A))]
    for i in range(len(A)):
        for j in range(len(B[0])):
            for k in range(len(B)):
                result[i][j] += A[i][k] * B[k][j]
    return result

#
# time_cost = []
#
# for i in range(30):  # run 30 times
#     print('calculating round: ', i)
#     a = np.random.randint(0, 100, (288, 100))
#     b = np.random.randint(0, 100, (100, 366))
#     start = time.time()
#     c = matrixMultiply(a,b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

# Time cost as followed:  [7.102163076400757, 6.707699298858643, 6.667217016220093, 6.710032939910889, 6.68825101852417, 6.745857000350952, 6.717119932174683, 6.724790811538696, 6.6010260581970215, 6.634565114974976, 6.63702392578125, 6.601875066757202, 6.667417049407959, 6.69169807434082, 6.6340179443359375, 7.20841383934021, 6.861229181289673, 6.596113920211792, 6.615758895874023, 6.69171404838562, 6.827954053878784, 6.712049245834351, 6.745818853378296, 11.924853086471558, 6.991988182067871, 6.756083965301514, 6.824030160903931, 6.84436297416687, 6.7350897789001465, 6.838403701782227]
# Average time cost is:  6.9234872738520306


# Double matrix

# time_cost = []
# 
# for i in range(30):
#     print('calculating round: ', i)
#     a = np.random.rand(288, 100)
#     b = np.random.rand(100, 366)
#     start = time.time()
#     c = matrixMultiply(a, b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
# 
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

# Time cost as followed:  [7.111241102218628, 6.802486181259155, 6.731729745864868, 6.780375957489014, 6.841831922531128, 6.832874059677124, 6.791154146194458, 6.798407077789307, 6.7124269008636475, 6.7132580280303955, 6.797778129577637, 6.7693092823028564, 6.806358098983765, 6.716917991638184, 6.7373948097229, 6.656981945037842, 6.701997995376587, 6.75265097618103, 6.673988103866577, 6.750978708267212, 6.763760805130005, 6.738003969192505, 6.725855827331543, 7.0771472454071045, 6.951946973800659, 6.948734998703003, 6.674102067947388, 7.226620197296143, 6.849748134613037, 6.805507183074951]
# Average time cost is:  6.8080522855122885
