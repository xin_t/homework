import numpy as np
import time

'''
Platform One

Hardware & Software environment: 
CPU: Intel i7-9700kf     8 cores @ 3.6Ghz  Turbo Boost @ 4.9Ghz
Memory: 32GB @ 3200 Mhz 
OS: Windows Server 2019
IDE: PyCharm 2020.2 Professional
Interpreter: Python 3.7

'''


# generates 2 integer matrices and multiply several times, record execution time
# Idea: use for loop instead of numpy method

# Integer matrix

def matrixMultiply(A, B):
    result = [[0] * len(B[0]) for i in range(len(A))]
    for i in range(len(A)):
        for j in range(len(B[0])):
            for k in range(len(B)):
                result[i][j] += A[i][k] * B[k][j]
    return result


# time_cost = []
#
# for i in range(30):  # run 30 times
#     print('calculating round: ', i)
#     a = np.random.randint(0, 100, (288, 100))
#     b = np.random.randint(0, 100, (100, 366))
#     start = time.time()
#     c = matrixMultiply(a,b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

# result: Time cost as followed:  [6.2922203540802, 6.3200647830963135, 6.26543927192688, 6.305983543395996,
# 6.311420202255249, 6.279085397720337, 6.306584596633911, 6.271026849746704, 6.2778825759887695, 6.298670530319214,
# 6.272697448730469, 6.304621696472168, 6.25396203994751, 6.284199476242065, 6.290061712265015, 6.263593435287476,
# 6.311980724334717, 6.282068252563477, 6.307402849197388, 6.325168132781982, 6.271358013153076, 6.315746545791626,
# 6.260785818099976, 6.273892164230347, 6.266078233718872, 6.2615745067596436, 6.285691022872925, 6.226297616958618,
# 6.257589340209961, 6.263812065124512]
# Average time cost is:  6.2835653066635135


# Double matrix

# time_cost = []
#
# for i in range(30):
#     print('calculating round: ', i)
#     a = np.random.rand(288, 100)
#     b = np.random.rand(100, 366)
#     start = time.time()
#     c = matrixMultiply(a, b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

# result：Time cost as followed:  [6.5757856369018555, 6.564128398895264, 6.564813852310181, 6.563031435012817,
# 6.524222373962402, 6.5240232944488525, 6.538941383361816, 6.519085168838501, 6.519461393356323, 6.509734153747559,
# 6.492799520492554, 6.493149995803833, 6.506347417831421, 6.4902379512786865, 6.496951341629028, 6.498167037963867,
# 6.484147071838379, 6.487752199172974, 6.499687433242798, 6.4782493114471436, 6.488426685333252, 6.494274616241455,
# 6.478233575820923, 6.485914468765259, 6.4928669929504395, 6.471778869628906, 6.479560613632202, 6.488074779510498,
# 6.473007678985596, 6.479024887084961]
# Average time cost is:  6.505395984649658
