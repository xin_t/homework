# Each line in the file has two fields: the first field indicates
# what kind of operation the CPU performs (read, write, etc.),
# and the second field is the address.
# Here is what the number in the first field means:
# 0: read data
# 1: write data
# 2: instruction fetch


# draw the histogram of address distribution for each of them (2x20 points).
# On the Ox axis of the plot you will have the address number (don't start with zero,
# rather with the smallest address you find in the file and go up to the
# maximum address in the file).
# On the Oy axis you will have the number of occurrences for each particular address.

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib


# open the target file
f = open(r'C:\Users\Administrator\Desktop\IIT\2020秋季学期\402 Introduction to Advance Studies\HW\HW1\spice.din')

# turn file information into list
f_to_list = f.read().split('\n')

# construct a list of address
list_of_address = []
for s in f_to_list:
    list_of_address.append(s[2:])
list_of_address.pop()  # delete last empty element


# change list into decimal and sort
list_of_address_deci = []
for l in list_of_address:
    list_of_address_deci.append(int(l,16))

list_of_address_deci.sort()

data_deci = list_of_address_deci




# change decimal list back to hex list
sorted_address_hex = []
for h in list_of_address_deci:
    sorted_address_hex.append(hex(h))
data = sorted_address_hex
# df = pd.DataFrame(data)
# df.to_csv('address_hex_sorted.csv')


# address_hex_dict = {}
# for a in data:
#     address_hex_dict[a] = address_hex_dict.get(a, 0) + 1
# df = pd.DataFrame(pd.Series(address_hex_dict),columns=['count'])
# df = df.reset_index().rename(columns={'index':'address'})
# df.to_csv('address_hex_sorted_occurence.csv')

# count occurrences and find out the largest count
# address_dict = {}
# for a in data:
#     address_dict[a] = address_dict.get(a, 0) + 1
# largest_count = max(address_dict.values())
# print(largest_count)  # result = 12965
# print(len(address_dict))  # result = 15320

# 0x40044c  0x7ffebb5c


# plt.hist(data, bins=15320, facecolor="blue", edgecolor="black", alpha=0.7)
# plt.xlabel("Address")
# plt.ylabel("Counts")
# plt.title("Address Histogram")
# plt.show()

'''
# construct a list of instruction
list_of_instruction = []
for i in f_to_list:
    list_of_instruction.append(i[0:1])

# count the frequency of elements in list, store the result in diction
instruction_dict = {}
for key in list_of_instruction:
    instruction_dict[key] = instruction_dict.get(key, 0) + 1

# result: {'2': 782764, '0': 150699, '1': 66538, '': 1}

# a = 782764/(782764+150699+66538)
# b = 150699/(782764+150699+66538)
# c = 66538/(782764+150699+66538)
# print(a,c,b)
# result: 0.7827632172367828 0.06653793346206653 0.1506988493011507
# frequency:
# instruction fetch  2 -> 78.3%
# write data  1 -> 6.7%
# read data  0 -> 15.1%
'''