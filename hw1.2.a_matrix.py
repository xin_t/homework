import numpy as np
import time

'''
Platform One

Hardware & Software environment: 
CPU: Intel i7-9700kf     8 cores @ 3.6Ghz  Turbo Boosted @ 4.9Ghz
Memory: 32GB @ 3200 Mhz 
OS: Windows Server 2019
IDE: PyCharm 2020.2 Professional
Interpreter: Python 3.7

'''

# generates 2 integer matrices and multiply several times, record execution time

# Integer matrix
#
# time_cost = []
#
# for i in range(30):
#     print('calculating round: ', i)
#     a = np.random.randint(0, 1000, (2000, 1000))
#     b = np.random.randint(0, 1000, (1000, 2000))
#     start = time.time()
#     c = np.dot(a, b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))
# Result: [4.798677206039429, 5.02905535697937, 4.998190641403198, 4.8814697265625, 4.930378198623657,
# 4.974271774291992, 4.686080694198608, 4.9244372844696045, 4.948357105255127, 4.716148614883423, 4.89310359954834,
# 4.988871812820435, 4.93145227432251, 4.701661109924316, 4.838260889053345, 5.129474401473999, 5.195270776748657,
# 4.8168158531188965, 4.97339653968811, 5.074131727218628, 4.771963834762573, 4.96245265007019, 5.079161167144775,
# 4.86997127532959, 4.963500499725342, 4.959490537643433, 4.726129770278931, 4.947104215621948, 4.944120407104492,
# 4.7211620807647705]
# Average time cost is:  4.912485400835673

# Double matrix

# time_cost = []
#
# for i in range(30):
#     print('calculating round: ', i)
#     a = np.random.rand(9999, 8888)
#     b = np.random.rand(8888, 9999)
#     start = time.time()
#     c = np.dot(a, b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

# Result: Time cost as followed:  [4.018192291259766, 5.068885803222656, 4.455016613006592, 5.0903143882751465,
# 4.422132253646851, 4.964166879653931, 4.596633434295654, 5.020528793334961, 4.727787494659424, 4.997076988220215,
# 4.947202682495117, 5.227468729019165, 4.7033514976501465, 5.0344557762146, 4.851950645446777, 5.133742809295654,
# 4.751733303070068, 4.953221321105957, 4.732783079147339, 4.980151891708374, 4.701371908187866, 4.8160481452941895,
# 4.647011041641235, 5.008528232574463, 4.641030311584473, 4.982112884521484, 4.903323650360107, 4.992119550704956,
# 4.722301244735718, 5.081850290298462]
# Average time cost is:  4.8390831311543785