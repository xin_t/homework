# Each line in the file has two fields: the first field indicates
# what kind of operation the CPU performs (read, write, etc.),
# and the second field is the address.
# Here is what the number in the first field means:
# 0: read data
# 1: write data
# 2: instruction fetch


# draw the histogram of address distribution for each of them (2x20 points).
# On the Ox axis of the plot you will have the address number (don't start with zero,
# rather with the smallest address you find in the file and go up to the
# maximum address in the file).
# On the Oy axis you will have the number of occurrences for each particular address.

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib


# open the target file
f = open(r'C:\Users\Administrator\Desktop\IIT\2020秋季学期\402 Introduction to Advance Studies\HW\HW1\tex.din')

# turn file information into list
f_to_list = f.read().split('\n')



# construct a list of address
list_of_address = []
for s in f_to_list:
    list_of_address.append(s[2:])



# # change list into decimal and sort
# list_of_address_deci = []
# for l in list_of_address:
#     list_of_address_deci.append(int(l,16))
#
# list_of_address_deci.sort()
#
#
#
# # change decimal list back to hex list
# sorted_address_hex = []
# for h in list_of_address_deci:
#     sorted_address_hex.append(hex(h))
# data = sorted_address_hex
# df = pd.DataFrame(data)
# df.to_csv('address_hex_sorted_tex.csv')
#
#
# address_hex_dict = {}
# for a in data:
#     address_hex_dict[a] = address_hex_dict.get(a, 0) + 1
# df = pd.DataFrame(pd.Series(address_hex_dict),columns=['count'])
# df = df.reset_index().rename(columns={'index':'address'})
# df.to_csv('address_hex_sorted_occurence_tex.csv')

# count occurrences and find out the largest count
# address_dict = {}
# for a in data:
#     address_dict[a] = address_dict.get(a, 0) + 1
# largest_count = max(address_dict.values())
# print(largest_count)
# print(len(address_dict))





# construct a list of instruction
list_of_instruction = []
for i in f_to_list:
    list_of_instruction.append(i[0:1])

# count the frequency of elements in list, store the result in diction
# instruction_dict = {}
# for key in list_of_instruction:
#     instruction_dict[key] = instruction_dict.get(key, 0) + 1
#
# print(instruction_dict)
# result: {'2': 597309, '1': 104513, '0': 130655}

a = 597309/(597309+104513+130655)
b = 104513/(597309+104513+130655)
c = 130655/(597309+104513+130655)
print(a,b,c)
# result: 0.7175081113352081 0.156947279023925 0.12554460964086694
# frequency:
# instruction fetch  2 -> 71.8%
# write data  1 -> 12.6%
# read data  0 -> 15.7%
