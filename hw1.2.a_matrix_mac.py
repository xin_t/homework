import numpy as np
import time

'''
Platform Two

Hardware & Software environment: 
CPU: Intel i9-9980H     8 cores @ 2.3Ghz  Turbo Boot @ 4.8Ghz
Memory: 16GB @ 2666 Mhz 
OS: MacOS
IDE: PyCharm 2020.2 Professional
Interpreter: Python 3.7

'''

# generates 2 integer matrices and multiply several times, record execution time

# Integer matrix
#
time_cost = []

for i in range(30):
    print('calculating round: ', i)
    a = np.random.randint(0, 1000, (2000, 1000))
    b = np.random.randint(0, 1000, (1000, 2000))
    start = time.time()
    c = np.dot(a, b)
    end = time.time()
    cost = end - start
    time_cost.append(cost)

print('Time cost as followed: ', time_cost)
print('Average time cost is: ', np.mean(time_cost))

# Time cost as followed:  [6.556530237197876, 6.403412818908691, 6.791450262069702, 6.335788011550903,
# 6.3495001792907715, 6.8242833614349365, 6.455524921417236, 6.491732835769653, 6.882962942123413, 6.4183189868927,
# 6.418220043182373, 6.8430609703063965, 6.442033052444458, 6.4189629554748535, 6.83442497253418, 6.4631547927856445,
# 6.493613004684448, 6.897916078567505, 6.739139795303345, 6.468449115753174, 6.867062091827393, 6.422466039657593,
# 6.467481851577759, 6.837738990783691, 6.426811218261719, 6.489684104919434, 9.207902908325195, 6.4469780921936035,
# 6.267145872116089, 6.699273347854614]
# Average time cost is:  6.655367461840312


# Double matrix

# time_cost = []
#
# for i in range(30):
#     print('calculating round: ', i)
#     a = np.random.rand(9999, 8888)
#     b = np.random.rand(8888, 9999)
#     start = time.time()
#     c = np.dot(a, b)
#     end = time.time()
#     cost = end - start
#     time_cost.append(cost)
#
# print('Time cost as followed: ', time_cost)
# print('Average time cost is: ', np.mean(time_cost))

Time cost as followed:  [7.983824968338013, 5.554580926895142, 5.460465908050537, 5.4734718799591064,
5.487864017486572, 5.497779130935669, 5.480170011520386, 5.488614797592163, 5.493377923965454, 5.5129499435424805,
5.506233215332031, 5.50065279006958, 5.462610960006714, 5.5039167404174805, 5.458966970443726, 5.470710039138794,
5.468487739562988, 5.525515079498291, 5.507030010223389, 5.514909982681274, 5.516764879226685, 5.5254621505737305,
5.498000860214233, 5.506489038467407, 5.49360203742981, 5.505590200424194, 5.494624853134155, 5.501842021942139,
5.505148887634277, 5.5211021900177]
# Average time cost is:  5.580692005157471